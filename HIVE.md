cd /home/Jig11309
hadoop fs -mkdir movie
hadoop fs -put u.data movie/
hadoop fs -ls
hadoop fs -ls movie/
CREATE DATABASE cts_jig11309;
USE cts_jig11309;

1.

CREATE EXTERNAL TABLE IF NOT EXISTS u_data (
user_id INT,
item_id INT,
rating INT,
time_stamp BIGINT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LINES TERMINTAED BY '\n'
STORED AS TEXTFILE
LOCATION '/movie/data';																	

2.

DESCRIBE u_data;

3.

SELECT * FROM u_data;

4.

SELECT user_id,COUNT(item_id) FROM u_data GROUP BY user_id ORDER BY user_id;

5.

SELECT item_id,COUNT(user_id) FROM u_data GROUP BY item_id ORDER BY item_id;


hadoop fs -mkdir movie1
hadoop fs -put u.user movie1/

6.

CREATE EXTERNAL TABLE IF NOT EXISTS u_user1 ( 
userId INT, 
age INT, 
gender STRING, 
occupation STRING, 
zip INT 
) 
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|' 
LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION '/user/Jig11309/movie1';

7.

DESCRIBE u_user1;

8.

SELECT * FROM u_user1;

9.

SELECT COUNT(*) FROM u_user1;

10.


SELECT gender,COUNT(gender) FROM u_user1 GROUP BY gender;

11.

SELECT /*+ MAPJOIN (u_user1) */ usr.userId, usr.age,uda.rating FROM u_user1 usr join u_data uda ON usr.userId = uda.user_id;

 

SELECT usr.userid, usr.age,uda.rating FROM u_user1 usr join u_data uda ON usr.userid = uda.user_id;

 
12.

CREATE TABLE u_user_partitioned ( 
userId INT, 
age INT, 
gender STRING, 
zip INT 
) PARTITIONED BY (occupation STRING);
set hive.exec.dynamic.partition.mode=nonstrict;
INSERT OVERWRITE TABLE u_user_partitioned PARTITION(occupation) SELECT userId,age,gender,zip,occupation FROM u_user1;

13.

select cnt.occupation as occupation, count(*) as counter from u_user_partitioned cnt group by cnt.occupation;

administrator   79
artist  28
doctor  7
educator        95
engineer        67
entertainment   18
executive       32
healthcare      16
homemaker       7
lawyer  12
librarian       51
marketing       26
none    9
other   105
programmer      66
retired 14
salesman        12
scientist       31
student 196
technician      27
writer  45
Time taken: 12.716 seconds

SELECT gender, COUNT(*) FROM u_user_partitioned WHERE occupation = 'other' GROUP BY gender;

F       36
M       69
Time taken: 12.382 seconds

select cnt.occupation as occupation, count(*) as counter from u_user1 cnt group by cnt.occupation;

administrator   79
artist  28
doctor  7
educator        95
engineer        67
entertainment   18
executive       32
healthcare      16
homemaker       7
lawyer  12
librarian       51
marketing       26
none    9
other   105
programmer      66
retired 14
salesman        12
scientist       31
student 196
technician      27
writer  45
Time taken: 10.32 seconds

SELECT gender, COUNT(*) FROM u_user1 WHERE occupation = 'other' GROUP BY gender;


F       36
M       69
Time taken: 14.354 seconds
