library(XML)
library(RCurl)
theurl <- "C:\\Data Science with R\\Assignments\\Graded Assignments\\Topic 6.2 -  Data Visualization in R\\The World's Most Valuable Brands List - Forbes.html"
tables <- readHTMLTable(theurl)
str(tables)
topBrands <- as.data.frame(tables$the_list)
#topBrands <- unlist(lapply(tables,function(t) dim(t)[1]))
#t1 <- tables[[which.max(topBrands)]]
#output <- data.frame(t1)
output$Company.Advertising <- gsub("[$]","",output$Company.Advertising)
output$Company.Advertising <- gsub("M","/1000",output$Company.Advertising)
library(xlsx)
write.xlsx(output, "C:\\Jig11309\\T6.2\\exporting.xlsx")
#cleaning the file and importing the file
#symbols were removed for visulisation purpose
library(XLConnect)
brands <- loadWorkbook("C:\\Jig11309\\T6.2\\manipulated.ods")
brands_new <- readWorksheet(brands,sheet = "Sheet1", header = TRUE)
class(brands_new)
library(dplyr)
industry_mani <- filter(brands_new, Industry %in% c("Technology"))
library(ggplot2)
names(industry_mani)
#first
p <- ggplot(industry_mani, aes(x = Company_Advertising,y = Brand_Revenue,label = Brand))
p + geom_point(aes(colour = Brand, size = Brand_Value))+geom_text(cex = 4, hjust = 0.7, vjust = -1)+xlab("Company Advertising in billions of '$'")+ylab("Brand Revenue in billions of '$'")+ggtitle("Industry")+xlim(0,5)
str(industry_mani)
?'geom_text'
#second
luxury_industry <- filter(brands_new, Industry %in% c("Luxury"))
luxury_industry1 <- subset(luxury_industry,Company_Advertising>0 & Company_Advertising<6)
names(luxury_industry)
p <- ggplot(luxury_industry1, aes(x = Company_Advertising,y = Brand_Revenue,label = Brand))
p + geom_point(aes(colour = Brand, size = Brand_Value))+geom_text(cex = 4, hjust =  0.7, vjust = -1)+xlab("Company Advertising in billions of '$'")+ylab("Brand Revenue in billions of '$'")+ggtitle("Luxury")+theme(panel.grid.minor = element_line(colour = "grey",size = 0.1))+scale_x_continuous(breaks = seq(0,6,0.1))
#third
financial_industry <- filter(brands_new, Industry %in% c("Financial Services"))
financial_industry1 <- subset(financial_industry,Company_Advertising>0 & Company_Advertising<6)
p <- ggplot(financial_industry1, aes(x = Company_Advertising,y = Brand_Revenue,label = Brand))
p + geom_point(aes(colour = Brand, size = Brand_Value))+geom_text(cex = 4, hjust =  0, vjust = 0)+xlab("Company Advertising in billions of '$'")+ylab("Brand Revenue in billions of '$'")+ggtitle("Financial Services")+theme(panel.grid.minor = element_line(colour = "grey",size = 0.1))+scale_x_continuous(breaks = seq(0,6,0.1))
#fourth
automotive_industry <- filter(brands_new, Industry %in% c("Automotive"))
automotive_industry1 <- subset(automotive_industry,Company_Advertising>0 & Company_Advertising<6)
p <- ggplot(automotive_industry1, aes(x = Company_Advertising,y = Brand_Revenue,label = Brand))
p+geom_point(aes(colour = Brand, size = Brand_Value))+geom_text(cex = 4, hjust =  0, vjust = 0)+xlab("Company Advertising in billions of '$'")+ylab("Brand Revenue in billions of '$'")+ggtitle("Automotive")

