library(dplyr)
library(irr)
library(rpart)
library(caret)
library(rattle)
library(rpart.plot)
library(RColorBrewer)
data_imp <- read.csv("C:\\Data Science with R\\Assignments\\Graded Assignments\\Topic 11.2 -  Decison Trees\\BH.csv")
summary(data_imp)
summary(data_imp$cus_employername)
#summary(data_imp$GOOD_BAD)
summary(data_imp$TARGET)
unique(data_imp$TARGET)
#data_imp$TARGET1 <- ifelse(is.na(data_imp$TARGET),"Missing",as.character(data_imp$TARGET))
data_imp1 <- filter(data_imp,TARGET %in% c(1,0))
#data_imp$TARGET1 <- as.factor(data_imp$TARGET1)
#data_imp <- subset(data_imp,,-c(id,TARGET))
unique(data_imp1$TARGET)
mod <- rpart(TARGET~cus_employername,data = data_imp,control = rpart.control(cp=0.001),method="class")
mod
data_imp1$node <- mod$where
table(mod$where)
table(mod$where)/22888
data_imp1[,"id_node"] <- mod$where
data_imp1%>%mutate(Group=ifelse(id_node==2,1,2))->data_imp1
head(table(data_imp1$cus_employername,data_imp1$Group))

Output -

                       1 2
                       1 0
   Allsaints           0 1
   Cariilion/ Explorer 0 1
   Peninsula           0 1
   The Mansion House   1 0
   Village Homecare    1 0
